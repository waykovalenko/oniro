# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

##
## Submits a job to LAVA with CI variables
##
.lava-test:
  interruptible: true
  image:
    name: registry.ostc-eu.org/ostc/oniro/bitbake-builder:latest
  variables:
    GIT_STRATEGY: none
    CI_LAVA_INSTANCE: "https://lava.ostc-eu.org/"
    # Run the same ltp test suites that using by LKFT project.
    # Reference: https://qa-reports.linaro.org/lkft/linux-stable-rc-linux-5.10.y/build/v5.10.98-75-g9f5cb871ceb9/testjobs/
    LTP_TST_CMDFILES: "cap_bounds cpuhotplug crypto nptl pty securebits fs controllers hugetlb mm tracing containers io dio syscalls math commands ipc fcntl-locktests filecaps fs_bind fs_perms_simple fsx sched cve"
  before_script:
    - test -n "$CI_LAVA_TOKEN" || (
        echo "precondition failed - please disable the child job if CI_LAVA_TOKEN not set in gitlab CI/CD variables"
        && exit 1 )
    # Check if the job is configured properly.
    - test -n "$MACHINE" || (
        echo "precondition failed - set MACHINE to the name of the target device for which the image is built"
        && exit 1 )
    - test -n "$CI_BUILD_JOB_NAME" || (
        echo "precondition failed - set CI_BUILD_JOB_NAME to the appropriate job name from which LAVA will pick up build artifact"
        && exit 1 )
    - test -n "$CI_LAVA_JOB_DEFINITION" || (
        echo "precondition failed - set CI_LAVA_JOB_DEFINITION to the URL of the LAVA test job definition"
        && exit 1 )
    - test -n "$CI_REPORT_JOB_NAME" || (
        echo "precondition failed - set CI_REPORT_JOB_NAME to the CI job name which will gather results back from LAVA"
        && exit 1 )
  script:
    # Build callback URL for the "report" job
    - curl --silent "$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs?per_page=100" > jobs-manual.json
    - job_id="$(jq -r ".[] | select(.name == \"$CI_REPORT_JOB_NAME\") | .id" jobs-manual.json)"
    - build_job_id="$(jq -r ".[] | select(.name == \"$CI_BUILD_JOB_NAME\") | .id" jobs-manual.json)"
    - CALLBACK_URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/${job_id}/play"
    # Get the job definition from remote source.
    - curl --silent "$CI_LAVA_JOB_DEFINITION" > job_def.yaml
    # Update the job definition with CI data.
    - sed -i -e 's/@ci_job_id@/'"$CI_JOB_ID"'/'
      -e 's/@ci_project_id@/'"$CI_PROJECT_ID"'/'
      -e 's/@ci_pipeline_id@/'"$CI_PIPELINE_ID"'/'
      -e 's,@ci_pipeline_url@,'"$CI_PIPELINE_URL"','
      -e 's/@build_job_id@/'"$build_job_id"'/'
      -e 's,@callback_url@,'"$CALLBACK_URL"',' job_def.yaml
    # Generate test jobs.
    - |
      set -x
      rm -rf lava_jobs && mkdir lava_jobs
      # Generate job definitions for zephyr build
      if echo "${CI_BUILD_JOB_NAME}" | grep -i "zephyr"; then
        for image in $(find artifacts/images/${MACHINE} -name "*.elf" -exec basename {} \;); do
          image_basename=$(basename "${image}" ".elf")
          echo "--- Generating lava job definition ${image_basename}.yaml ---"
          sed "s/@image_basename@/$image_basename/" job_def.yaml | tee lava_jobs/"${image_basename}".yaml
        done
      # Generate job definitions for linux LTP test.
      elif grep -q "@TST_CMDFILES@" job_def.yaml; then
        for test_cmdfile in ${LTP_TST_CMDFILES}; do
          ltp_job_name="ltp-${test_cmdfile}.yaml"
          echo "--- Generating lava job definition ${ltp_job_name} ---"
          sed "s/@TST_CMDFILES@/${test_cmdfile}/" job_def.yaml | tee lava_jobs/"${ltp_job_name}"
        done
      else
        mv job_def.yaml lava_jobs/
      fi
      set +x
    # Submit the job to LAVA.
    - |
      for job_def in $(find lava_jobs/ -name "*.yaml"); do
        lava_job_id=$(curl -X POST -H "Authorization: Token $CI_LAVA_TOKEN" -F "definition=$(<${job_def})" "$CI_LAVA_INSTANCE/api/v0.2/jobs/" | jq ".job_ids" | tr -d "[\n ]")
        if [ "${lava_job_id}" != "null" ]; then
          echo "$CI_LAVA_INSTANCE/scheduler/job/$lava_job_id"
          echo "$lava_job_id" >> job_ids_"${CI_JOB_NAME}_${lava_job_id}".txt
        else
          echo "Failed to submit ${job_def}"
          exit 1
        fi
      done
  artifacts:
    paths:
      - job_ids_*.txt
      - lava_jobs/*.yaml
  rules:
    # Run the build when it is scheduled.
    - if: $CI_PIPELINE_SOURCE == "schedule"
    # Do not run pipelines for draft merge requests unless manually triggered.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(wip|draft):.*/i'
      when: manual
    # Run the build when a merge request is created.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    # Run the build when a tag is placed.
    - if: '$CI_COMMIT_TAG'

.lava-report:
  interruptible: true
  # Let the pipeline continue running 'lava-badge' job.
  allow_failure: true
  image:
    name: registry.ostc-eu.org/ostc/oniro/bitbake-builder:latest
  stage: report
  variables:
    GIT_STRATEGY: none
    CI_LAVA_INSTANCE: "https://lava.ostc-eu.org/"
  script:
    - |
      incomplete=false
      for file in $(find ./ -name "job_ids_*.txt"); do
        echo "Job file: $file"
        while read -r p; do
          echo "Job ID: $p"
          mkdir "$p"
          # Echo LAVA job health and link.
          link="$CI_LAVA_INSTANCE/scheduler/job/$p"
          health="$(curl --silent "$CI_LAVA_INSTANCE/api/v0.2/jobs/$p/" | jq '.health' | tr -d '"')"
          echo $health | grep -iq incomplete && incomplete=true
          echo "[$health] $link"
          # Get the JUnit export from LAVA.
          curl --silent -o job_$p.xml "$CI_LAVA_INSTANCE/api/v0.2/jobs/$p/junit/?classname_prefix=$p"
          echo "Report file: $(ls job_$p.xml)"
        done < "$file"
      done
      if $incomplete; then
        echo "ERROR: Incomplete test job reported: exit code 1"
        exit 1
      fi
  artifacts:
    when: always
    paths:
      - job_*.xml
    reports:
      junit:
        - job_*.xml
  rules:
    # Run the build when it is scheduled.
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: manual
    # Do not run pipelines for draft merge requests unless manually triggered.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TITLE =~ /^(wip|draft):.*/i'
      when: manual
    # Run the build when a merge request is created.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: manual
    # Run the build when a tag is placed.
    - if: '$CI_COMMIT_TAG'
      when: manual

.lava-badge:
  interruptible: true
  image:
    name: registry.ostc-eu.org/ostc/oniro/bitbake-builder:latest
  stage: report
  variables:
    GIT_STRATEGY: none
  script:
    - |
      set -x
      curl --silent "$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/test_report" > test-report.json
      total_count="$(jq '.total_count' test-report.json)"
      success_count="$(jq '.success_count' test-report.json)"
      success_rate=$(( 100 * success_count / total_count ))
      anybadge --label=lava-test --value=${success_rate} --suffix='%' --file=lava-test.svg 50=red 60=orange 80=yellow 100=green
      set +x
  artifacts:
    when: always
    paths:
      - lava-test.svg
  rules:
    # Run the build when it is scheduled.
    - if: $CI_PIPELINE_SOURCE == "schedule"
